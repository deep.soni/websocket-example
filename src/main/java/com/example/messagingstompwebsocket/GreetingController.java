package com.example.messagingstompwebsocket;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.util.HtmlUtils;

@Controller
public class GreetingController {

	@Autowired
	SimpMessagingTemplate simpMessagingTemplate;

	@MessageMapping("/subscribe/{symbol}")
	// @SendTo("/topic/greetings/{symbol}")
	public void greeting(@DestinationVariable String symbol, SubscribeMessage message) throws Exception {
		System.out.println("symbol:: " + symbol);
		// simpMessagingTemplate.convertAndSend("/topic/greetings/" + symbol, "Symbol
		// Name: " + message.getSymbolName());
		// return new Greeting("Symbol Name: " +
		// HtmlUtils.htmlEscape(message.getSymbolName()) + "!");
	}

	@Scheduled(fixedRate = 5000L)
	public void sendPong() {
		System.out.println("Scheduled");
		simpMessagingTemplate.convertAndSend("/topic/greetings/dru", new Greeting("Symbol Name: "));
	}

}
