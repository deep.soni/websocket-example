package com.example.messagingstompwebsocket;

public class SubscribeMessage {

	private String symbolName;

	public SubscribeMessage() {
	}

	public SubscribeMessage(String symbolName) {
		this.symbolName = symbolName;
	}

	public String getSymbolName() {
		return symbolName;
	}

	public void setSymbolName(String symbolName) {
		this.symbolName = symbolName;
	}

}
